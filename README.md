# newPod

[![CI Status](https://img.shields.io/travis/kirankrify/newPod.svg?style=flat)](https://travis-ci.org/kirankrify/newPod)
[![Version](https://img.shields.io/cocoapods/v/newPod.svg?style=flat)](https://cocoapods.org/pods/newPod)
[![License](https://img.shields.io/cocoapods/l/newPod.svg?style=flat)](https://cocoapods.org/pods/newPod)
[![Platform](https://img.shields.io/cocoapods/p/newPod.svg?style=flat)](https://cocoapods.org/pods/newPod)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

newPod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'newPod'
```

## Author

kirankrify, alekhya@krify.com

## License

newPod is available under the MIT license. See the LICENSE file for more info.
